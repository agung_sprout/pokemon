import React , {Suspense,lazy} from 'react'

// Store Management
import { Provider } from 'react-redux'
import {BrowserRouter , Route, Switch} from 'react-router-dom'
import configureStore from './store/ContainerReducer'

// Routing
import routes from './config/routes'

// Layout
const MainLayout = lazy(()=>import('./views/layouts/MainLayout'))

const loading = () => (<p className={{textAlign:'center'}}>loading ...</p>)
function App() {
  return (
    <Provider store={configureStore()}>
      <BrowserRouter>
        <Switch>
            <Suspense fallback={loading()}>
                {routes.map((route,i)=>{
                  return route.component ? (
                      <Route
                        key={i}
                        path={route.path}
                        exact={route.exact}
                        render={props => (
                          <MainLayout component={route.component} {...props}/>
                        )}
                      />
                  ) : (null)
                })}
            </Suspense>
        </Switch>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
