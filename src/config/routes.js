import {lazy} from 'react'

const PokemonList = lazy(()=>import('../views/containers/pokomeon/PokemonList'))
const PokemonDetail =  lazy(()=>import('../views/containers/pokomeon/PokemonDetail'))
const PokemonCatched =  lazy(()=>import('../views/containers/pokomeon/PokemonCatched'))


const routes = [
    {path:'/',component:PokemonList,exact:true},
    {path:'/detail/:id',component:PokemonDetail,exact:false},
    {path:'/catched',component:PokemonCatched,exact:false}
]

export default routes