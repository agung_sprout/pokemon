import React, {useEffect,useState,lazy} from 'react'
import { useSelector } from 'react-redux'
import { useParams} from 'react-router-dom'
import { DetailPokemonListService } from '../../../services/PokemonService'
import {ArrayDescription} from '../../../helpers/ArrayDescription'

const PokemonSetUsernameComponent =  lazy(()=>import('../../components/pokemon/PokemonSetUsernameComponent'))
const PokemonDetail = props => {
    const [detail,setDetail] = useState({loadedData:false})
    const [catched,setCatched] = useState(0)

    let { id } = useParams()
    const pokemon = useSelector(store => store.pokemon)

    const catchPokemon = () => {
        const probabilityCatched = Math.random();
        if (probabilityCatched>=0.5) {
            setCatched(id)
        }else {
            alert('You are not catch it, try again!');
        }
    }
    useEffect(()=>{
        if(pokemon.pokemonList.length === 0){
            DetailPokemonListService(id).then(res => {
                setDetail({
                    img : res.sprites.front_default,
                    name : res.name,
                    type : res.types,
                    move : res.moves,
                    id : res.id,
                    loadedData : true
                })
            })
        }else{
            const pokemonDetail = pokemon.pokemonList.find(data => data.id === parseInt(id))
            setDetail(pokemonDetail)
        }
        
    },[id])
    return(
        <div className="textCenter detailPage">
            <div>
                <img src={detail.img} alt={detail.name}/>
                <h3>
                    {detail.name} 
                    {catched === 0 ? <button className="btnSubmit" disabled={!detail.loadedData} onClick={()=>catchPokemon()} type="button">catch</button> : null }
                </h3>
                {catched !== 0 ?  <PokemonSetUsernameComponent {...props} pokemonId={id} /> : null}
                <div style={{marginTop:15}}>
                    <h4>Moves :</h4>
                    <p>{ArrayDescription(detail.move,'move')}</p>
                </div>
                <div style={{marginTop:15}}>
                    <h4>Types :</h4>
                    <p>{ArrayDescription(detail.type,'type')}</p>
                </div>
            </div> 
        </div>
    )
}

export default PokemonDetail