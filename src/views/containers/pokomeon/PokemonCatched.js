import React , {lazy,Suspense} from 'react'

const PokemonCatchedSection = lazy(()=>import('../../section/pokemon/PokemonCatchedSection'))
const PokemonCatched = () => {
    return(
        <div>
            <Suspense fallback={<p className="textCenter">Loading ...</p>}>
                <PokemonCatchedSection />
            </Suspense>
        </div>
    )
}
export default PokemonCatched