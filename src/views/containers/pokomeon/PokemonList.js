import React , {lazy,Suspense} from 'react'

const PokemonListSection = lazy(()=>import('../../section/pokemon/PokemonListSection'))
const PokemonList = () => {
    return(
        <div>
            <Suspense fallback={<p className="textCenter">Loading ...</p>}>
                <PokemonListSection />
            </Suspense>
        </div>
    )
}
export default PokemonList