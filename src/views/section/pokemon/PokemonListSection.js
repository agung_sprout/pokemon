import React, {useEffect,lazy,Suspense} from 'react'

import {useDispatch, useSelector} from 'react-redux'
import { PokemonListService } from '../../../services/PokemonService'

const PokemonItemComponent = lazy(()=>import('../../components/pokemon/PokemonItemComponent'))

const PokemonListSection = () => {
    const dispatch = useDispatch()
    const pokemon = useSelector(store => store.pokemon)
    useEffect(()=>{
        PokemonListService(dispatch,pokemon).then()
    },[dispatch])
    return(
        <div className="pokemonList">
            {FetchDataPokemon()}
        </div>
    )
}

const FetchDataPokemon = () => {
    const pokemon = useSelector(store => store.pokemon)
    return pokemon.pokemonList.map((data,i)=>{
        return (
            <Suspense key={i} fallback={<p className="textCenter">Loading ...</p>}>
                <PokemonItemComponent {...data} />
            </Suspense>
            
        )
    })
}

export default PokemonListSection