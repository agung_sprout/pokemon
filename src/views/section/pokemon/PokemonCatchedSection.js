import React , {useEffect,useState,lazy,Suspense} from 'react'

import {useDispatch, useSelector} from 'react-redux'
import {PokemonListService,CatchedListPokemon,RemoveCatchedPokemon} from '../../../services/PokemonService'

const PokemonCatchedItemComponent = lazy(()=>import('../../components/pokemon/PokemonCatchedItemComponent'))

const PokemonCatchedSection = () => {
    const [loadPokemons,setLoadPokemons] = useState(false)
    const [pokemonCatched,setPokemonCatched] = useState([])

    const dispatch = useDispatch()
    const pokemon = useSelector(store => store.pokemon)
    useEffect(()=>{
        PokemonListService(dispatch,pokemon).then(res => {
            setLoadPokemons(res)
            CatchedListPokemon(pokemon.pokemonList).then(res => {
                setPokemonCatched(res)
            })
            
        })
        
    },[dispatch,pokemon.pokemonList])

    const RemovePokemon = (key,id,name) => {
        if(window.confirm(`Are you sure to delete ${name} ?`)){
            const loadPokemonCatched = [...pokemonCatched]
            const indexRemove = loadPokemonCatched.findIndex(data => data.number === key)
            RemoveCatchedPokemon(id,pokemon.pokemonList,dispatch).then(() => {
                loadPokemonCatched.splice(indexRemove,1)
                setPokemonCatched(loadPokemonCatched)
            })
        }
    }
    return(
        <div>
            {loadPokemons ? 
                <div className="pokemonList" style={{justifyContent:'space-around'}}>
                    {pokemonCatched.map((data,index)=> {
                        return(
                            <Suspense key={index} fallback={<p className="textCenter">Loading ...</p>}>
                                <PokemonCatchedItemComponent remove={RemovePokemon} index={index} {...data}/>
                            </Suspense>
                        )
                    })}
                </div>
            : 
                <p className="textCenter">loading ...</p>
            }
            {
                loadPokemons && pokemonCatched.length === 0 ? <p className="textCenter">- You have not pokemon :( -</p> : null
            }
        </div>
    )
}


export default PokemonCatchedSection