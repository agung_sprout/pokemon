import React, {useState} from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { CatchNewPokemon } from '../../../services/PokemonService';
const PokemonSetUsernameComponent = props => {
    const [nickname,setNickname] = useState('')
    const dispatch = useDispatch()
    const pokemon = useSelector(store => store.pokemon)
    const onSetUsernamePokemon = (e) => {
        e.preventDefault()
        CatchNewPokemon(props.pokemonId,nickname,dispatch,pokemon).then(() => {
            props.history.push('/catched')
        })
    }
    return (
        <div style={{margin:'10px 0'}}>
            <p style={{fontSize:12}} className="textCenter">Give name for your new pokemon</p>
            <form onSubmit={(e)=>onSetUsernamePokemon(e)}>
                <input onChange={(e)=>setNickname(e.target.value)} type="text"/>
                <button type="submit" className="btnSubmit">Save</button>
            </form>
           
        </div>
    )
}

export default PokemonSetUsernameComponent