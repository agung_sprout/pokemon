import React from 'react'
const PokemonCatchedItemComponent = props => {
    return(
        <div className="pokemonItem textCenter">
            <img src={props.img} alt={props.name} />
            <h5>{props.name}</h5>
            <button type="button" onClick={()=>props.remove(props.number,props.id,props.name)}>Remove</button>
        </div>
    )
}

export default PokemonCatchedItemComponent