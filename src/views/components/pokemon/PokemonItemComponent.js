import React from 'react'
import { Link } from 'react-router-dom';
const PokemonItemComponent = props => {
    return(
        <Link className="itemToDetail" to={`/detail/${props.id}`}>
            <div className="pokemonItem textCenter">
                <img src={props.img} alt={props.name} />
                <h5>{props.name}</h5>
                <span>Owned : {props.owned}</span>
            </div>
        </Link>
    )
}

export default PokemonItemComponent