import React from 'react'
import {Link} from 'react-router-dom'
const MainLayout =  props => {
    return(
        <div className="app">
            <header>
                <div className="left">
                    <Link to="/">
                        <h3>POKEMON</h3>
                    </Link>
                </div>
                <div className="right">
                    <Link to="/catched">My Pokemon</Link>
                </div>
            </header>
            <content>
                <props.component {...props}/>
            </content>
            <footer>

            </footer>
        </div>
    )
}

export default MainLayout