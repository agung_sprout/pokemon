import { combineReducers } from 'redux'
import pokemonReducer from './reducers/PokemonReducer'
export default combineReducers({
    pokemon : pokemonReducer
});