import { SET_LIST_POKEMON, SET_DETAIL_POKEMON } from '../actions/PokemonAction'

const initialState = {
    pokemonList : []
}
export default (state = initialState, action) => {
    switch (action.type) {
        case SET_LIST_POKEMON : 
            return {
                ...state,
                pokemonList : action.pokemons
            }
        case SET_DETAIL_POKEMON :
            let pokemonList = [...state.pokemonList]
            const indexFocus = pokemonList.findIndex(data => data.id === action.pokemon.id)
            pokemonList[indexFocus] = {
                ...pokemonList[indexFocus],
                img : action.pokemon.sprites.front_default,
                loadedData : true,
                move : action.pokemon.moves,
                type : action.pokemon.types
            }
            return {
                ...state,
                pokemonList
            }
        default :
            return {
                ...state
            }
    }
}
