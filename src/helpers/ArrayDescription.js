export const ArrayDescription = (arr,type) => {
    let data = ''
    if(arr !== undefined){
        arr.map((item, key) => {
            data+=`${item[type].name}` + (key < arr.length - 1 ? ', ' : '.');
            return true
        })
    }else{
        data = 'loading ...'
    }
    return data
}