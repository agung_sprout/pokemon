import axios from 'axios'

import { SET_LIST_POKEMON, SET_DETAIL_POKEMON } from '../store/actions/PokemonAction'
const CACHE_NAME = 'pokemonCached'

const PokemonListService = async(dispatch,pokemon) => {
    const pokemons = []
    if(pokemon.pokemonList.length === 0){
        await axios('https://pokeapi.co/api/v2/pokemon').then(res=>{
            for(let i = 0; i < 20; i++){
                pokemons.push({
                    id: i + 1,
                    name: res.data.results[i].name,
                    img: '',
                    move: '',
                    type: '',
                    owned: 0,
                    loadedData : false
                })
            }
        })
        dispatch({type:SET_LIST_POKEMON,pokemons})
        for(let i = 1; i <= 20; i++){
            DetailPokemonListService(i).then(res => {
                dispatch({type:SET_DETAIL_POKEMON,pokemon:res})
            })
        }
    }

    const pokemonCatched = JSON.parse((localStorage.getItem(CACHE_NAME) !== null ? localStorage.getItem(CACHE_NAME) : '[]'))
    for(let i = 0; i < pokemonCatched.length; i++){
        const index = pokemons.findIndex(data => data.id === parseInt(pokemonCatched[i].id))
        if(index !== -1){
            pokemons[index].owned += 1
        }
    }
    return true
}

const DetailPokemonListService = async(id) => {
    const pokemonDetail = await axios(`https://pokeapi.co/api/v2/pokemon/${id}`)
    return pokemonDetail.data
}

const CatchNewPokemon = async(id,nickname,dispatch,pokemon) => {
    const catchedPokemon = await localStorage.getItem(CACHE_NAME)
    const cachePokemon = (catchedPokemon === null ? [] : JSON.parse(catchedPokemon))
    
    cachePokemon.push({id,nickname})
    localStorage.setItem(CACHE_NAME,JSON.stringify(cachePokemon))
    
    const pokemons = [...pokemon.pokemonList]
    const index = pokemons.findIndex(data => data.id === parseInt(id))
    pokemons[index].owned += 1
    dispatch({type:SET_LIST_POKEMON,pokemons})

    return true
}

const CatchedListPokemon = async(pokemon) => {
    const pokemonCatched = []

    if(pokemon.length > 0){
        const catchedPokemon = await localStorage.getItem(CACHE_NAME)
        const cachePokemon = (catchedPokemon === null ? [] : JSON.parse(catchedPokemon))
        
        for(let i = cachePokemon.length - 1; i >= 0; i--){
            const indexPokemon = pokemon.findIndex(data => data.id === parseInt(cachePokemon[i].id))
            pokemonCatched.push({
                id : pokemon[indexPokemon].id,
                name : cachePokemon[i].nickname,
                img : pokemon[indexPokemon].img,
                number:i
            })
        }
    }
    return pokemonCatched
}
const RemoveCatchedPokemon = async(id,pokemons,dispatch) => {
    const catchedPokemon = await JSON.parse(localStorage.getItem(CACHE_NAME))
    const indexRemove = catchedPokemon.findIndex(data => parseInt(data.id) === id)
    catchedPokemon.splice(indexRemove,1)
    localStorage.setItem(CACHE_NAME,JSON.stringify(catchedPokemon))

    // Remove from store
    const index = pokemons.findIndex(data => data.id === parseInt(id))
    pokemons[index].owned -= 1
    dispatch({type:SET_LIST_POKEMON,pokemons})
    return true
}
export {
    PokemonListService,
    DetailPokemonListService,
    CatchNewPokemon,
    CatchedListPokemon,
    RemoveCatchedPokemon
}